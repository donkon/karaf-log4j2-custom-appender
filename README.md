# Build custom bundle

```
me@localhost:~/opt/apache-karaf-4.2.6$ mkdir -p system/de/example/my-appender/1.0.0-SNAPSHOT
me@localhost:~/projects/my-appender$ mvn clean package && cp target/my-appender-1.0.0-SNAPSHOT.jar ~/opt/apache-karaf-4.2.6/system/de/example/my-appender/1.0.0-SNAPSHOT/
```

# etc/startup.properties

Add bundle to startup.properties

```
mvn\:de.example/my-appender/1.0.0-SNAPSHOT = 7
```

# org.ops4j.pax.logging.cfg

Add custom appender

```
log4j2.appender.myappender.type = MyAppender
log4j2.appender.myappender.name = MyAppender
log4j2.appender.myappender.layout.type = PatternLayout
log4j2.appender.myappender.layout.pattern = ${log4j2.out.pattern}
log4j2.appender.myappender.jndiName=osgi:service/foo
```

# Start Karaf

Following exception comes up:

```
me@localhost:~/opt/apache-karaf-4.2.6$ ./bin/karaf 
2019-09-05 10:21:31,821 CM Configuration Updater (Update: pid=org.ops4j.pax.logging) ERROR Unable to locate plugin type for MyAppender
2019-09-05 10:21:31,993 CM Configuration Updater (Update: pid=org.ops4j.pax.logging) ERROR Unable to locate plugin for MyAppender
2019-09-05 10:21:31,994 CM Configuration Updater (Update: pid=org.ops4j.pax.logging) ERROR Unable to invoke factory method in class class org.apache.logging.log4j.core.config.AppendersPlugin for element Appenders. java.lang.NullPointerException
	at org.apache.logging.log4j.core.config.plugins.visitors.PluginElementVisitor.visit(PluginElementVisitor.java:52)
	at org.apache.logging.log4j.core.config.plugins.util.PluginBuilder.generateParameters(PluginBuilder.java:248)
	at org.apache.logging.log4j.core.config.plugins.util.PluginBuilder.build(PluginBuilder.java:131)
	at org.apache.logging.log4j.core.config.AbstractConfiguration.createPluginObject(AbstractConfiguration.java:952)
	at org.apache.logging.log4j.core.config.AbstractConfiguration.createConfiguration(AbstractConfiguration.java:892)
	at org.apache.logging.log4j.core.config.AbstractConfiguration.doConfigure(AbstractConfiguration.java:508)
	at org.apache.logging.log4j.core.config.AbstractConfiguration.initialize(AbstractConfiguration.java:232)
	at org.apache.logging.log4j.core.config.AbstractConfiguration.start(AbstractConfiguration.java:244)
	at org.apache.logging.log4j.core.LoggerContext.setConfiguration(LoggerContext.java:545)
	at org.apache.logging.log4j.core.LoggerContext.start(LoggerContext.java:261)
	at org.ops4j.pax.logging.log4j2.internal.PaxLoggingServiceImpl.doUpdate(PaxLoggingServiceImpl.java:213)
	at org.ops4j.pax.logging.log4j2.internal.PaxLoggingServiceImpl.updated(PaxLoggingServiceImpl.java:158)
	at org.ops4j.pax.logging.log4j2.internal.PaxLoggingServiceImpl$1ManagedPaxLoggingService.updated(PaxLoggingServiceImpl.java:426)
	at org.apache.felix.cm.impl.helper.ManagedServiceTracker.updated(ManagedServiceTracker.java:189)
	at org.apache.felix.cm.impl.helper.ManagedServiceTracker.updateService(ManagedServiceTracker.java:152)
	at org.apache.felix.cm.impl.helper.ManagedServiceTracker.provideConfiguration(ManagedServiceTracker.java:85)
	at org.apache.felix.cm.impl.ConfigurationManager$UpdateConfiguration.run(ConfigurationManager.java:1400)
	at org.apache.felix.cm.impl.UpdateThread.run0(UpdateThread.java:138)
	at org.apache.felix.cm.impl.UpdateThread.run(UpdateThread.java:105)
	at java.lang.Thread.run(Thread.java:748)

2019-09-05 10:21:31,996 CM Configuration Updater (Update: pid=org.ops4j.pax.logging) ERROR Unable to locate appender "PaxOsgi" for logger config "root"
2019-09-05 10:21:31,997 CM Configuration Updater (Update: pid=org.ops4j.pax.logging) ERROR Unable to locate appender "RollingFile" for logger config "root"
2019-09-05 10:21:31,997 CM Configuration Updater (Update: pid=org.ops4j.pax.logging) ERROR Unable to locate appender "Console" for logger config "root"
2019-09-05 10:21:31,997 CM Configuration Updater (Update: pid=org.ops4j.pax.logging) ERROR Unable to locate appender "AuditRollingFile" for logger config "audit"
```