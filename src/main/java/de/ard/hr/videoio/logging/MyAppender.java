package de.ard.hr.videoio.logging;

import java.io.Serializable;

import org.apache.logging.log4j.core.Appender;
import org.apache.logging.log4j.core.Core;
import org.apache.logging.log4j.core.Filter;
import org.apache.logging.log4j.core.Layout;
import org.apache.logging.log4j.core.LogEvent;
import org.apache.logging.log4j.core.appender.AbstractAppender;
import org.apache.logging.log4j.core.config.Property;
import org.apache.logging.log4j.core.config.plugins.Plugin;
import org.apache.logging.log4j.core.config.plugins.PluginAttribute;
import org.apache.logging.log4j.core.config.plugins.PluginElement;
import org.apache.logging.log4j.core.config.plugins.PluginFactory;

/**
 * Schreibt Log-Meldungen in die VIO-Datenbank.
 * 
 * @author bjoern.konrad@hr.de
 * 
 */
@Plugin(name = "MyAppender", category = Core.CATEGORY_NAME, elementType = Appender.ELEMENT_TYPE)
public class MyAppender extends AbstractAppender {
	
	private String jndiName;

	protected MyAppender(String name, Filter filter, Layout<? extends Serializable> layout, boolean ignoreExceptions, Property[] properties) {
		super(name, filter, layout, ignoreExceptions, properties);
	}

	protected MyAppender(String name, Filter filter, Layout<? extends Serializable> layout, boolean ignoreExceptions, String jndiName) {
		super(name, filter, layout, ignoreExceptions, Property.EMPTY_ARRAY);
		this.jndiName = jndiName;
	}

	@PluginFactory
	public static MyAppender createAppender(@PluginAttribute("name") String name, @PluginAttribute("jndiName") String jndiName, @PluginElement("Filter") Filter filter, @PluginElement("Filter") Layout<? extends Serializable> layout) {
		return new MyAppender(name, filter, layout, false, jndiName);
	}

	@Override
	public void append(LogEvent event) {
		System.out.println("Received log event: " + event);
	}

	public String getJndiName() {
		return jndiName;
	}

	public void setJndiName(String jndiName) {
		this.jndiName = jndiName;
	}

}
